﻿Set-Alias -Name gdfq -Value Get-DirectoryFilesQueue
Function Get-DirectoryFilesQueue{
param([Parameter(Mandatory = $true, Position = 0)][string]$path,
[switch]$recurse,
[switch]$files,
[switch]$directory)
$processingqueue = New-Object -Type System.Collections.Queue
$rootpath = [System.IO.DirectoryInfo]$path
$filecount = 0
$processingqueue.Enqueue($rootpath)
while($processingqueue.Count -gt 0){
    $current = $processingqueue.Dequeue()
    try{
        if($files.IsPresent -or -not($files.IsPresent -bxor $directory.IsPresent)){
            foreach($file in $current.EnumerateFiles()){
                if(($file.Attributes -band [System.IO.FileAttributes]::System) -ne [System.IO.FileAttributes]::System){
                        $file
                    }
            }
        }
        
        foreach($dir in $current.EnumerateDirectories()){
            #Avoid iterating over system files
            if((($dir.Attributes -band [System.IO.FileAttributes]::System) -ne [System.IO.FileAttributes]::System) -and 
               #Avoid iterating over reparse points
              ($dir.Attributes -band [System.IO.FileAttributes]::ReparsePoint) -ne [System.IO.FileAttributes]::ReparsePoint){
                if($directory.IsPresent -or -not($files.IsPresent -bxor $directory.IsPresent)){
                    $dir
                }
                if($recurse.IsPresent){
                    $processingqueue.Enqueue($dir)
                }
                
            }
        }
    }
    catch [System.UnauthorizedAccessException]{
        $error
    }

}
}